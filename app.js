'use strict';

const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const port = 8008;
const cors = require("cors");
const ApplePayHelper = require('apple-pay-helper');
const fs = require('fs');
const path = require('path');
const https = require('https');
const axios = require('axios');
require('dotenv').config();
const crypto_helper = require('./crypto-helper');

let corsOptions = {
    origin: "https://apple-pay-gateway-cert.apple.com"
};

const sandboxCertPem = fs.readFileSync(path.join(__dirname, './cert/certificate_sandbox.pem')).toString();
const sandboxKeyPem = fs.readFileSync(path.join(__dirname, './cert/certificate_sandbox.key')).toString();

const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
    cert: sandboxCertPem,
    key: sandboxKeyPem,
    // pfx: paymentProcessP12File,
    // passphrase: 'Test@123'
  });

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0');
    next();
});

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

app.get('/', (request, response) => {
    response.json({
        info: 'Node.js, Express, and Postgres API'
    })
});

/**
 * Apple Pay Merchant Validataion
 */

//With Axios
app.post('/validateSession3', async (req, res) => {
    try {
        console.log("IP", req.ip);
        const response = await axios.post(
            'https://apple-pay-gateway-cert.apple.com/paymentservices/paymentSession',
            {
                merchantIdentifier: 'merchant.com.arada.superapp',
                displayName: 'supperAppApple',
                initiative: 'web',
                initiativeContext: 'intranet.accionlabs.com'
            },
            { httpsAgent }
        );

        res.status(200).send({   status: true,"message": "Success", data: response.data});
    } catch (error) {
        console.log("Error", error);
        res.status(500).send({ status: false, data: [], error });
    }
});
//


/**
 * PayFort Payment Service
 */
app.post('/handlePayment/applePay', async (req, res, next) => {
    try {
        if (req.body) {
            let payload = req.body;
            console.log(req.body);
            let options = {
                "digital_wallet":"APPLE_PAY",
                "command":"PURCHASE",
                "access_code": process.env.access_code,
                "merchant_identifier": process.env.merchant_identifier,
                "merchant_reference": "Test" + Math.random().toString(36).substring(2, 10),
                "amount": (parseFloat(payload.amount) * 100).toString(),
                "currency": payload.currency,
                "language":"en",
                "customer_email":"test@example.com",
                "apple_data": payload.paymentData.data,
                "apple_signature": payload.paymentData.signature,
                "apple_header": {
                    "apple_transactionId": payload.paymentData.header.transactionId,
                    "apple_ephemeralPublicKey": payload.paymentData.header.ephemeralPublicKey,
                    "apple_publicKeyHash": payload.paymentData.header.publicKeyHash
                },
                "apple_paymentMethod":{
                    "apple_displayName": payload.paymentMethod.displayName,
                    "apple_network": payload.paymentMethod.network,
                    "apple_type": payload.paymentMethod.type
                },
                "customer_ip": payload.customer_ip,
                "signature": ""
            };

            let payload_signature = await new crypto_helper().calculate(options, process.env.sha_request_phrase, "sha256");
            
            options.signature =  payload_signature;

            const response = await axios.post('https://sbpaymentservices.payfort.com/FortAPI/paymentApi', options, {
                headers: {
                  'Content-Type': 'application/json'
                }
            });

            console.log("Response of Payfort", response.data);
            res.status(200).send({   status: true,"message": "Success", data: response.data});
        } else {
            res.status(500).send({ status: false, data: [], error: "Request Payload not found." });
        }
    } catch (error) {
        console.log("Error", error);
        res.status(500).send({ status: false, data: [], error });
    }
});
//

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
});