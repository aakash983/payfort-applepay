const crypto = require('crypto');


class CryptoHelper {

    /**
     * Calcualte Signature
     */
    async calculate(unsorted_dict, sha_phrase, sha_method = 'sha256') {
        const mainKeys = Object.keys(unsorted_dict);
        const sorted_keys = mainKeys.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));
        const sorted_dict = sorted_keys.reduce((acc, key) => {
            if (typeof unsorted_dict[key] === 'object' && !Array.isArray(unsorted_dict[key])) {
                acc[key] = unsorted_dict[key];
            } else {
                acc[key] = unsorted_dict[key];
            }
            return acc;
        }, {});

        let result = '';

        for (const [key, val] of Object.entries(sorted_dict)) {
            if (key === 'signature') {
                continue;
            }
            if (typeof val !== 'object') {
                result += `${key}=${val}`;
            } else {
                let sub_str = `${key}={`;
                let index = 0;
                const keys = Object.keys(val);
                for (const k of keys) {
                    sub_str += `${k}=${val[k]}`;
                    if (index < keys.length - 1) {
                        sub_str += ', ';
                    }
                    index++;
                }
                sub_str += '}';
                result += sub_str;
            }
        }

        const result_string = `${sha_phrase}${result}${sha_phrase}`;

        const signature = crypto.createHash(sha_method).update(result_string).digest('hex');
        return signature;
    }

    /**
     * generateSignature using private key
     */
    async generateSignature(params, privateKey) {
        const sortedParams = Object.keys(params).sort().map(key => `${key}=${params[key]}`).join('&');
        console.log("sortedParams", sortedParams);
        const signature = crypto.createHmac('sha256', privateKey).update(sortedParams).digest('hex');

        return signature;
    }
}

module.exports = CryptoHelper;